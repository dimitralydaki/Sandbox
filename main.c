#include "GenericList.h"
#include "GenericListEx.h"
#include "fsm.h"
#include "themata.h"
#include "vop.h"

int main(int argc, char* argv[]) {
    // FSMApp();
    // ThemataApp();
    // VOPAppProblem();
    // VOPApp();
    GenericListApp();
    // GenericListExApp();
    return 0;
}
