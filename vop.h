#ifndef _VOP_H_
#define _VOP_H_

typedef void (*Praxi)(void* res, void* a, void* b);
void vop(void* a[], void* b[], void* c[], int size, Praxi praxi);
void VOPApp();

#endif
