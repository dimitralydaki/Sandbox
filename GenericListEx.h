//Don't forget header guards
//Don't Forget to call GenericListApp in main
//Don't forget to add Makefile dependencies
struct Node {
    void* value;
    struct Node* next;
}
//typedef to Node

struct List {
    Node* head;
};
//typedef to List

//typedef ForEachFuncPtr is pointer to func returning nothing with params, _value is ptr to anything
//typedef MapFuncPtr is pointer to func returning nothing with params, res is ptr to anything, _value is read only ptr to anything
//typedef ReduceFuncPtr is pointer to func returning nothing with params, total is ptr to anything, _value is read only ptr to anything

//Dynamicaly allocates new list
List* ListCreate(void);

//Frees memory held by list
void ListDestroy(List* list);

//Insert new element to list
void ListInsert(List* list, const void* _value);

//Invokes funtion named foreach, For Each of the list values
void ListForEach(List* list, ForEachFuncPtr foreach);

//Invokes funtion named map, For Each of the list values and saves the result to the corresponding resList element
//Stop iterating when list or resList size is reached
void ListMap(List* list, List* resList, MapFuncPtr map);

//Invokes funtion named reduce, For Each of the list values and saves the result to total. total is used as function parameter in the next iteration
void ListReduce(List* list, void* total, ReduceFuncPtr reduce);

//Invoke in main
void GenericListExApp();
