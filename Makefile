CC=gcc
CFLAGS=-O0 -g -Wall#-ansi -pedantic #-NDEBUG
LIBS=
LPATHS=
INCLUDES=fsm.h themata.h vop.h GenericList.h #GenericListEx.h
# define the C source files
SRCS=main.c fsm.c themata.c vop.c GenericList.c #GenericListEx.c
# suffix replacement $(name:string1=string2)
OBJS=$(SRCS:.c=.o)
MAIN=main

.PHONY: clean

all : $(MAIN)
	@echo 'Sandbox Compiled :)'

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LPATHS) $(LIBS)

# Create object files of source code
%.o: %.c $(INCLUDES)
	$(CC) $(CFLAGS) -o $@ -c $<

# Clean object files
clean:
	$(RM) *.o
	$(RM) $(MAIN)
