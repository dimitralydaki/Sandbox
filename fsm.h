#ifndef _FSM_H_
#define _FSM_H_
//Google 'Header File Guard' for the above symbols

// enum State { START,
//              EXPLORE,
//              ATTACK,
//              FLEE,
//              STATE_MAX };
// typedef enum State State;

//This is the same as the two commented-out stametemnts above
typedef enum State { START = 0,
                     EXPLORE,
                     ATTACK,
                     FLEE,
                     STATE_MAX } State;

typedef enum Input { INPUT_START,
                     SEE_PLAYER,
                     NO_PLAYER,
                     LOW_HEALTH,
                     HIGH_HEALTH,
                     INPUT_MAX } Input;

//Each callback is mapped to One state in our FSM
//The input parameter is the next action taken.
//If the next action causes a state change, the new state is returned, else the same state(function name) is returned
//Number of callbacks must always be the same as the size of the 'State' type (minus STATE_MAX)
//Defined in source file fsm.c
State StartCallback(Input input);
State ExploreCallback(Input input);
State AttackCallback(Input input);
State FleeCallback(Input input);

typedef State (*StateCallback)(Input input);

//Declaration Only(NOT Definition) of our 'action' Dispatch Table
//Defined in source file fsm.c
extern StateCallback action[];

//Usage:
//  make
//  ./main < in.txt
void FSMApp();
#endif
