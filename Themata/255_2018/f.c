#include <stdio.h>

#include "g.c"
char* N1 = "game1";
char* N2 = "game2";
int A1 = 20;
int A2 = 5;
int threshold = 18;

void f(char* name, int old, int t) {
    if (old > t)
        printf(" %s is %d years old\n", name, old);
    return;
}

int main​(void) {
    f(N2, A2, threshold);
    g(threshold);
    return 0;
}
