#include "vop.h"

#include <stdio.h>
#include <stdlib.h>

#define SIZE 5

void vop(void* a[], void* b[], void* c[], int size, Praxi praxi) {
    for (int i = 0; i < size; ++i)
        (*praxi)(c[i], a[i], b[i]);
}

// static void prosthesiLong(void* res, void* a, void* b) {
//     long* _res = (long*)res;    //void* -> long*
//     long* _a = (long*)a;
//     long* _b = (long*)b;
//     *_res = *_a + *_b;
// }
// static void prosthesiInt(void* res, void* a, void* b) {
//     int* _res = (int*)res;
//     int* _a = (int*)a;
//     int* _b = (int*)b;
//     *_res = *_a + *_b;
// }
// static void afairesiInt(void* res, void* a, void* b) {
//     int* _res = (int*)res;
//     int* _a = (int*)a;
//     int* _b = (int*)b;
//     *_res = *_a - *_b;
// }
static void prosthesiDouble(void* res, void* a, void* b) {
    double* _res = (double*)res;
    double* _a = (double*)a;
    double* _b = (double*)b;
    *_res = *_a + *_b;
}
// static void afairesiDouble(void* res, void* a, void* b) {
//     double* _res = (double*)res;
//     double* _a = (double*)a;
//     double* _b = (double*)b;
//     *_res = *_a - *_b;
// }

void VOPApp() {
    printf("Sizeof void* %ld\n", sizeof(void*));
    printf("Sizeof int* %ld\n", sizeof(int*));
    printf("Sizeof long* %ld\n", sizeof(long*));
    printf("Sizeof double* %ld\n", sizeof(double*));
    printf("Sizeof int %ld\n", sizeof(int));
    printf("Sizeof long %ld\n", sizeof(long));
    printf("Sizeof double %ld\n", sizeof(double));

    // long _a[SIZE] = { 1, 2, 3, 4, 5 };
    // long _b[SIZE] = { 1, 2, 3, 4, 5 };
    // long _c[SIZE] = { 0, 0, 0, 0, 0 };

    // int _a[SIZE] = { 1, 2, 3, 4, 5 };
    // int _b[SIZE] = { 1, 2, 3, 4, 5 };
    // int _c[SIZE] = { 0, 0, 0, 0, 0 };

    double _a[SIZE] = { 1.0, 2.0, 3.0, 4.0, 5.0 };
    double _b[SIZE] = { 1.0, 2.0, 3.0, 4.0, 5.0 };
    double _c[SIZE] = { 0.0, 0.0, 0.0, 0.0, 0.0 };

    double* a[SIZE] = { &_a[0], &_a[1], &_a[2], &_a[3], &_a[4] };
    double* b[SIZE] = { &_b[0], &_b[1], &_b[2], &_b[3], &_b[4] };
    double* c[SIZE] = { &_c[0], &_c[1], &_c[2], &_c[3], &_c[4] };
    vop((void**)a, (void**)b, (void**)c, SIZE, prosthesiDouble);
    for (int i = 0; i < SIZE; ++i)
        printf("%lf ", *(c[i]));
    printf("\n");
}
