#include "GenericList.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

List* ListCreate(void) {
    List* l = calloc(1, sizeof(List));
    l->head = NULL;
    return l;
}
void ListDestroy(List* list) {
    assert(list);
    Node* prev = NULL;
    for (Node* n = list->head; n != NULL; n = n->next) {
        free(prev);
        prev = n;
    }
    free(prev);
    free(list);
}
void ListInsert(List* list, const void* _value) {
    assert(list);
    Node* n = calloc(1, sizeof(Node));
    n->value = (void*)_value;
    n->next = list->head;
    list->head = n;
}

void ListForEach(List* list, ForEachFuncPtr foreach) {
    assert(list && foreach);
    for (Node* n = list->head; n != NULL; n = n->next)
        (*foreach)(n->value);
}
void ListMap(List* list, List* resList, MapFuncPtr map) {
    assert(list && resList && map);
    Node* rn = resList->head;
    for (Node* n = list->head; n != NULL && rn != NULL; n = n->next) {
        (*map)(rn->value, n->value);
        rn = rn->next;
    }
}
void ListReduce(List* list, void* total, ReduceFuncPtr reduce) {
    assert(list && total && reduce);
    for (Node* n = list->head; n != NULL; n = n->next) {
        (*reduce)(total, n->value);
    }
}

static void PrintNode(void* _value) {
    printf("%lf ", (*(double*)_value));
}

static void Power2(void* _value) {
    (*(double*)_value) = (*(double*)_value) * (*(double*)_value);
}

static void Negative(void* res, const void* _value) {
    (*(double*)res) = (-1.0) * (*(double*)_value);
}

static void Sum(void* total, const void* _value) {
    (*(double*)total) += (*(double*)_value);
}

#define SIZE 5
#define SIZE2 10
void GenericListApp() {
    List* l = ListCreate();
    double a[SIZE] = { 0.0 };
    for (int i = 0; i < SIZE; ++i) {
        a[i] = (double)SIZE - i;
        ListInsert(l, &a[i]);
    }
    ListForEach(l, PrintNode);
    printf("\n");

    ListForEach(l, Power2);
    ListForEach(l, PrintNode);
    printf("\n");

    double zeros[SIZE2] = { 0.0 };
    List* negList = ListCreate();
    for (int i = 0; i < SIZE2; ++i) {
        ListInsert(negList, &zeros[i]);
    }
    ListMap(l, negList, Negative);
    ListForEach(negList, PrintNode);
    printf("\n");

    double total = 0.0;
    ListReduce(l, &total, Sum);
    printf("sum: %lf\n", total);

    ListDestroy(negList);
    ListDestroy(l);
}
